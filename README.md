## Universe Chat

### Usage


In top navbar add some like this:



```
#!html
<li class="dropdown">
    <a href="#" class="dropdown-toggle navbar-icons" data-toggle="dropdown">
        <img src="/pic/messages.png" data-toggle="tooltip" title="{{i18n 'navbar.messages'}}">
        <em class="danger notif-count">

            {{> uniChatDropdownCount}}
        </em>
    </a>

    {{> uniChatDropdown}}
</li>
```

On top of layout add:


```
#!html
<div id="dock" class="dock uni-chat-widget-container">
    {{> uniChatWidgetList}}
</div>
```

In less put something like this:


```
#!css
.dock {
    position: fixed;
    bottom: 0;
    right: 0;
    z-index: 999;
}
```

Add ````js-main-header```` and ````js-main-footer```` classes to your header and footer. It is needed for inbox scrolling calculations.

