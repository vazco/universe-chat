'use strict';

Package.describe({
    summary: 'Chat (facebook style) for Universe CMS',
    name: 'vazco:universe-chat',
    version: '1.0.2'
});

Package.onUse(function (api) {
    api.use([
        'underscore',
        'tracker',
        'session',
        'iron:router',
        'vazco:universe-core',
        'vazco:universe-core-plugin',
        'aldeed:simple-schema',
        'raix:handlebar-helpers@0.2.4',
        'mizzao:user-status@0.6.4',
        'vazco:universe-update-operators-on-document@1.0.1'
    ]);

    api.use([
        'templating',
        'less'
    ], 'client');

    api.addFiles([
        'plugin.js',
        'lib/Chatrooms.js',
        'lib/Messages.js',
        'lib/methods.js'
    ]);

    api.addFiles([
        'client/helpers.js',

        'client/button.html',
        'client/button.js',

        'client/dropdown.html',
        'client/dropdown.js',
        'client/dropdown.less',

        'client/inbox.html',
        'client/inbox.js',
        'client/inbox.less',

        'client/widget.html',
        'client/widget.js',

        'client/router.js'
    ], 'client');

    api.addFiles([
        'server/publications.js'
    ], 'server');

    api.export('UniChat');
});
