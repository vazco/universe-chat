'use strict';

var UniMessage = UniCollection.UniDoc.extend();

_.extend(UniMessage.prototype, {
    getAuthor: function () {
        return UniUsers.findOne(this.user_id);
    },
    getChatroom: function () {
        return UniChat.Chatrooms.findOne(this.chatroom_id);
    },
    isOwn: function () {
        return this.user_id === Meteor.userId();
    }
});

UniChat.addCollection('Messages', {
    docConstructor: UniMessage,
    onInit: function (collection) {

        _(collection).extend({
            getUnreadedMessages: function (userId) {
                userId = userId || Meteor.userId();
                return UniChat.Messages.find({
                    user_ids: userId,
                    unreaded: userId
                });
            },
            getUserMessages: function (userId){
                userId = userId || Meteor.userId();
                return this.find({user_ids: userId});
            }
        });

        collection.attachSchema(new SimpleSchema({
            chatroom_id: {
                type: String,
                denyUpdate: true
            },
            user_id: {
                type: String,
                denyUpdate: true
            },
            user_ids: {
                type: [String],
                optional: true,
                denyUpdate: true
            },
            unreaded: {
                type: [String],
                optional: true
            },
            text: {
                type: String,
                max: 250,
                denyUpdate: true
            },
            ts: {
                type: Date,
                autoValue: function () {
                    if (this.isInsert) {
                        return new Date();
                    }
                },
                denyUpdate: true
            }
        }));
    }
});