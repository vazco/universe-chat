'use strict';

UniChat.addMethod('uniChatMethods', function (method, chatroomId, message) {
    this.unblock();

    var userId = this.userId;
    var chatroom = UniChat.Chatrooms.findOne(chatroomId);

    if (chatroom && _(chatroom.user_ids).contains(userId)) {
        switch (method) {
            case 'showChatToAll':
                chatroom._showChatToAll();
                break;

            case 'showChatToMe':
                chatroom._showChat(userId);
                break;

            case 'hideChatToMe':
                chatroom._hideChat(userId);
                break;

            case 'sendMessage':
                if(message){
                    chatroom._sendMessage(userId, message);
                }
                break;

            case 'markAsReaded':
                chatroom._markMessagesAsReaded(userId);
                break;
        }
    }
});