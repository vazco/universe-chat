'use strict';

var UniChatroom = UniCollection.UniDoc.extend();

_.extend(UniChatroom.prototype, {
    _sendMessage: function (userId, message) {
        this.showChatToAll();

        this.$set({
            last_message_ts: new Date()
        });

        return UniChat.Messages.insert({
            chatroom_id: this._id,
            user_id: userId,
            user_ids: this.user_ids,
            unreaded: _(this.user_ids).without(Meteor.userId()),
            text: message
        });
    },
    sendMessage: function (message) {
        Meteor.call('uniChatMethods', 'sendMessage', this._id, message);
    },
    getMessages: function () {
        return UniChat.Messages.find({chatroom_id: this._id});
    },
    getMessagesCount: function () {
        return this.getMessages().count();
    },
    getUnreadedMessages: function (userId) {
        return UniChat.Messages.find({
            chatroom_id: this._id,
            unreaded: userId
        }, {
            sort: {ts: 1}
        });
    },
    _markMessagesAsReaded: function (userId) {
        UniChat.Messages.update({
            chatroom_id: this._id,
            unreaded: userId
        }, {
            $pull: {unreaded: userId}
        }, {
            multi: true
        });
    },
    markAsReaded: function () {
        Meteor.call('uniChatMethods', 'markAsReaded', this._id);
    },
    _showChat: function (userId) {
        var user = UniUsers.findOne(userId);
        if (user && _.isObject(user.status) && user.status.online) {
            this.update({$addToSet: {opened_in: userId}});
        }
    },
    _showChatToAll: function () {
        var self = this;
        _(self.user_ids).each(function (userId) {
            self._showChat(userId);
        });
    },
    showChatToAll: function () {
        Meteor.call('uniChatMethods', 'showChatToAll', this._id);
    },
    showChatToMe: function () {
        Meteor.call('uniChatMethods', 'showChatToMe', this._id);
    },
    _hideChat: function (userId) {
        this.update({$pull: {opened_in: userId}});
    },
    hideChatToMe: function () {
        Meteor.call('uniChatMethods', 'hideChatToMe', this._id);
    },
    _getUsers: function (user_ids) {
        var users = [];
        _(user_ids).each(function (userId) {
            users.push(UniUsers.findOne(userId));
        });
        return users;
    },
    getUsers: function () {
        return this._getUsers(this.user_ids);
    },
    getUsersWithoutMe: function () {
        if (this.user_ids) {
            return this._getUsers(_(this.user_ids).without(UniUsers.getLoggedInId()));
        }
    },
    getTitle: function () {
        var users = this.getUsersWithoutMe();
        var title = '';
        if (users && users.length) {
            _(users).each(function (user) {
                if(user){
                    title += user.getName() + ' ';
                }
            });
        }
        return title;
    }
});

UniChat.addCollection('Chatrooms', {
    docConstructor: UniChatroom,
    onInit: function (collection) {

        _(collection).extend({
            createChatroomWithUsers: function (userIds) {
                userIds = userIds || [];
                userIds.push(Meteor.userId());
                userIds = _.unique(userIds);

                if (_.size(userIds) > 1) {

                    var chatroom = this.findOne({
                        user_ids: {
                            $all: userIds,
                            $size: _.size(userIds)
                        }
                    });
                    if (!chatroom) {
                        var id = this.insert({
                            user_ids: userIds
                        });
                        chatroom = this.findOne(id);
                    }
                    if(Meteor.isClient){
                        Session.set('uniChatActiveChatroom', chatroom._id);
                    }
                    return chatroom;
                }
            },
            getOpenedChatrooms: function (userId) {
                userId = userId || Meteor.userId();
                return this.find({opened_in: userId});
            },
            getUserChatrooms: function (userId) {
                userId = userId || Meteor.userId();
                return this.find({user_ids: userId}, {sort: {last_message_ts: -1}});
            }
        });

        if (Meteor.isClient) {
            collection.getActiveChatroom = function () {
                var chatroomId = Session.get('uniChatActiveChatroom'),
                    chatroom,
                    chatrooms,
                    userId = Meteor.userId();
                if (chatroomId) {
                    chatroom = this.findOne(chatroomId);
                    if (chatroom) {
                        chatroom.markAsReaded();
                        return chatroom;
                    }
                } else {
                    if(userId) {
                        chatrooms = this.find({user_ids: userId}, {
                            sort: {last_message_ts: -1},
                            limit: 1
                        }).fetch();
                        if (chatrooms[0]) {
                            chatrooms[0].markAsReaded();
                            return chatrooms[0];
                        }
                    }
                }
            };
        }

        collection.attachSchema(new SimpleSchema({
            user_ids: {
                type: [String],
                max: 20,
                optional: true,
                denyUpdate: true
            },
            opened_in: {
                type: [String],
                optional: true
            },
            created_date: {
                type: Date,
                optional: true,
                autoValue: function () {
                    if (this.isInsert || this.isUpsert) {
                        return new Date();
                    }
                },
                denyUpdate: true
            },
            last_message_ts: {
                type: Date,
                optional: true,
                autoValue: function () {
                    if (this.isUpdate) {
                        return new Date();
                    }
                }
            }
        }));

        collection.allow({
            insert: function () {
                return true;
            }
        });
    }
});