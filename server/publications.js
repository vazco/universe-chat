'use strict';

UniChat.addPublications({
    userMessages: function () {
        if (this.userId) {

            // TODO: optimize - don't publish all user messages.
            return [
                UniChat.Messages.getUserMessages(this.userId),
                UniChat.Chatrooms.getUserChatrooms(this.userId)
            ];
        }
    }
});