'use strict';

UniChat.addRoute('uniChatInbox', {
    path: '/inbox/:_id?',
    onRun: function () {
        var chatroom;
        var chatroomId;
        if (this.params._id) {
            Session.set('uniChatActiveChatroom', this.params._id);
        } else {
            chatroom = UniChat.Chatrooms.findOne();
            chatroomId = chatroom && chatroom._id;
            Session.set('uniChatActiveChatroom', chatroomId);
        }
        // old iron router api check
        if (typeof this.next === 'function') {
            this.next();
        }
    },
    onStop: function () {
        UniChat.Chatrooms.find().forEach(function (chat) {
            chat.hideChatToMe(Meteor.userId());
        });
    },
    data: function () {
        var chatroomsList = UniChat.Chatrooms.find({user_ids: {$in: [Meteor.userId()]}}, {sort: {last_message_ts: -1}});
        return {
            chatroomsList: chatroomsList
        };
    }
});
