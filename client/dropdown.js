'use strict';

UniChat.addEvents('uniChatDropdown', {
    'click .js-open-chatroom': function (e) {
        e.preventDefault();
        this.markAsReaded();
        this.showChatToMe();
    }
});

UniChat.addHelpers('uniChatDropdownCount', {
    'countUreaded': function () {
        return UniChat.Messages.getUnreadedMessages().count();
    }
});


