'use strict';

UniChat.addHelpers('uniChatInboxNavEntry', {
    isActive: function () {
        return Session.equals('uniChatActiveChatroom', this._id);
    },
    lastMsgFormatedDate: function () {
        var lastMsgDate = this.last_message_ts;
        var dateFormat;
        if (moment(new Date()).isSame(lastMsgDate, 'year')) {
            dateFormat = 'Do MMM HH:mm';
        } else {
            dateFormat = 'Do MMM YYYY HH:mm';
        }
        return moment(lastMsgDate).format(dateFormat);
    },
    unreadedMsgCounter: function () {
        var unreadedCountObj = this.getUnreadedMessages(Meteor.userId());
        var counter = unreadedCountObj && unreadedCountObj.count();
        if (counter > 0) {
            return counter;
        }
    },
    lastMsgText: function () {
        var textLength = 80;
        var sufix = '...';
        var getMsgs = UniChat.Messages.findOne({chatroom_id: this._id}, {sort: {ts: -1}});
        if(getMsgs){
            var msgText = getMsgs && getMsgs.text;
            if (msgText.length <= textLength) {
                sufix = '';
            }
            return msgText.substr(0, textLength) + sufix;
        }
        return '';

    }
});

Template.uniChatInbox.created = function () {
    this.activeUsersDropdownVar = new ReactiveVar(false);
};

UniChat.addHelpers('uniChatInbox', {
    usersInActiveChatRoom: function (limit) {
        var activeChatroom = Session.get('uniChatActiveChatroom');
        var activeChatroomObj;
        var users;
        if (activeChatroom) {
            activeChatroomObj = UniChat.Chatrooms.findOne(activeChatroom);
            users = activeChatroomObj && activeChatroomObj.getUsers(this.user_ids);
        }
        if (limit && users) {
            users = users.slice(0, limit);
        }
        return users;
    },
    activeUsersDropdown: function () {
        return Template.instance().activeUsersDropdownVar.get();
    }
});

UniChat.addEvents('uniChatInbox', {
    'click .js-go-back': function () {
        history.back();
    },
    'click .js-users-dropdown': function (e, tmpl) {
        var state = tmpl.activeUsersDropdownVar.get();
        tmpl.activeUsersDropdownVar.set(!state);
    }
});

UniChat.addEvents('uniChatInboxNavEntry', {
    'click .js-chatroom-entry': function (e, tmpl) {
        Session.set('uniChatActiveChatroom', this._id);
        // scroll down list
        tmpl.scrollTimeout = Meteor.setTimeout(function () {
            var div = document.querySelector('.messages');
            div.scrollTop = div.scrollHeight;
        }, 200);
    }
});

Template.uniChatInboxNavEntry.destroyed = function () {
    var self = this;
    if (self.scrollTimeout) {
        Meteor.clearTimeout(self.scrollTimeout);
    }
};
