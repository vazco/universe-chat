'use strict';

Template.uniChatWidget.created = function () {
    this.chatWidgetShrunken = new ReactiveVar(false);
};

UniChat.addEvents('uniChatWidget', {
    'click .js-hide-chat': function () {
        this.hideChatToMe();
    },
    'click .js-shrink-chat': function (e, tmpl) {
        var isShrunken = tmpl.chatWidgetShrunken.get();
        if (isShrunken) {
            tmpl.chatWidgetShrunken.set(false);
        } else {
            tmpl.chatWidgetShrunken.set(true);
        }
    },
    'click .js-chat-widget': function () {
        this.markAsReaded();
    }
});

UniChat.addHelpers('uniChatWidget', {
    chatWidgetShrunken: function () {
        return Template.instance().chatWidgetShrunken.get();
    }
});

UniChat.addHelpers('uniChatMessage', {
    fromNow: function (ts) {
        var now = moment();
        var ts = moment(ts);
        var dateFormat;
        if (now.diff(ts, 'hours') < 2) {
            return ts.fromNow('hh');
        }
        if (now.isSame(ts, 'year')) {
            dateFormat = 'Do MMM HH:mm';
        } else {
            dateFormat = 'Do MMM YYYY HH:mm';
        }
        return ts.format(dateFormat);
    }
});

UniChat.addHelpers('uniChatWidgetList', {
    isInbox: function () {
        var router = Router.current();
        var routerName;
        if (router && router.route) {
            // new Iron Router compatibility
            routerName = router.route.name || router.route.getName();
        }
        if (routerName === 'uniChatInbox') {
            return true;
        }
        return false;
    }
});

Tracker.autorun(function () {
    if (Meteor.userId()) {
        Meteor.subscribe('userMessages');
    }
});

UniChat.addEvents('uniChatForm', {
    'submit form': function (e) {
        e.preventDefault();
        var messageInput = $(e.target).find('.js-text').first();
        var message;
        if (messageInput) {
            message = messageInput.val();
            if (message) {
                this.sendMessage(message);
                messageInput.val('');
            }
        }
        return false;
    },
    'focus .js-text': function () {
        this.markAsReaded();
    }
});


// scrollers calculations
var calcMessagesHeight = function (tmpl, isNav) {
    // main header and footer heights - needed for fluid messagess scroll
    // you need to set proper classes in you project
    var topHeightSum = 0;
    var topBottomSum = 0;

    $('.js-main-header').each(function () {
        topHeightSum = topHeightSum + $(this).outerHeight(true);
    });
    $('.js-main-footer').each(function () {
        topBottomSum = topBottomSum + $(this).outerHeight(true);
    });

    var projectHeaderHeight = topHeightSum;
    var projectFooterHeight = topBottomSum;

    var uniChatToolbarHeight = $('.uni-chat-inbox-toolbar').outerHeight(true);
    var chatSearchHeight, navFooterHeight, navSearchHeight, sum, msgHeight;
    if (isNav) {
        navSearchHeight = $('.uni-chat-inbox-nav-search').outerHeight(true);
        navFooterHeight = $('.panel-inbox-nav-footer').outerHeight(true);
        sum = projectHeaderHeight + projectFooterHeight + uniChatToolbarHeight + navFooterHeight + navSearchHeight;
    } else {
        chatSearchHeight = $('.uni-chat-messages-list-form').outerHeight(true);
        sum = projectHeaderHeight + projectFooterHeight + uniChatToolbarHeight + chatSearchHeight;
    }

    msgHeight = $(window).height() - sum - 1;

    if (msgHeight < $(window).height() / 6) {
        msgHeight = $(window).height() / 2;
    }

    tmpl.$('.js-adjust-height').css('height', msgHeight);
};

Template.uniChatMessagesList.rendered = function () {
    var self = this;
    var chatid = Vazco.get(self, 'data._id');
    var $containerHeight;
    calcMessagesHeight(self, false);
    $(window).on('resize', function () {
        calcMessagesHeight(self, false);
    });
    Meteor.defer(function () {
        self.autorun(function () {
            if (UniChat.Messages.find({chatroom_id: chatid}).count()) {
                $containerHeight = self.$('.js-uni-chat-messages-container').outerHeight(true);
                $('.js-adjust-height.' + chatid).scrollTop($containerHeight + 100);
            }
        });
    });
};
Template.uniChatInboxNav.rendered = function () {
    var self = this;
    calcMessagesHeight(self, true);
    $(window).on('resize', function () {
        calcMessagesHeight(self, true);
    });
};
