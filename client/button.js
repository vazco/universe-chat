'use strict';

UniChat.addEvents('uniChatStartBtn', {
    'click .js-start-chat': function (e) {
        e.preventDefault();
        var user = UniUsers.findOne(this._id);
        var chatroom;
        if (user && _.isObject(user.status) && user.status.online) {
            chatroom = UniChat.Chatrooms.createChatroomWithUsers([this._id]);
            chatroom.showChatToMe();
        } else {
            chatroom = UniChat.Chatrooms.createChatroomWithUsers([this._id]);
            Router.go('/inbox/' + chatroom._id);
        }
    }
});
